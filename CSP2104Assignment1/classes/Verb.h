#ifndef VERB_H
#define VERB_H

#include <string>
#include "word.h"

class Verb :virtual public Word{

public:
	Verb(){};
	//Standard constructor that initializes all fields.
	Verb(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	//presumably this should return true, one would think.
	bool isVerb(){ return true; }
	std::string getDefinition(){ return "(v.) " + definition; }

};



#endif
