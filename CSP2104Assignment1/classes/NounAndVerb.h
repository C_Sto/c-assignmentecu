#ifndef NOUNANDVERB_H
#define NOUNANDVERB_H

#include <string>
#include "word.h"
#include "Verb.h"
#include "Noun.h"


class NounAndVerb : public Verb, public Noun{

public:
	NounAndVerb(){};
	//Standard constructor that initializes all fields.
	NounAndVerb(std::string w, std::string d){
		Noun::setWord(w);
		Noun::setDefinition(d);
	}
	using Noun::isNoun;
	using Verb::isVerb;

	std::string getDefinition(){ return "(n.v.) " + definition; }
};
#endif