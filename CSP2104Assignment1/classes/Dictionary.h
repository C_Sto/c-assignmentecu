#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <iostream>

#include "word.h"


//  The Dictionary class. Maintains a vector of word pointers, and performs various functions
class Dictionary{
private:
	//Vector of word pointers, may contain objects that inherit 'Word' properties.
	std::vector<std::unique_ptr<Word>> wordArray;
	int totalWords =0;

public:
	Dictionary(){}
	void loadDictionary();
	int getTotalNumberOfWords(){ return totalWords; }
	Word* findWord(std::string word);
	Word* basicTask1(std::string word);
	std::vector<std::string> basicTask2();
	std::vector<std::string> basicTask3();
	std::vector<std::string> basicTask4();
	std::vector<std::string>  basicTask5();
	std::vector<std::string>  basicTask6();
	std::string basicTask7();
	std::string intermediateTask1(std::string s);
	std::vector<std::string> intermediateTask2(std::string s);
	std::vector<std::string> intermediateTask3(std::string s);
	std::string advancedTask1();
	std::vector<std::string> advancedTask2();
};
#endif