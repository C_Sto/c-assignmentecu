#include "Dictionary.h"

#include <string>
#include "Dictionary.h"
#include "Noun.h"
#include "Verb.h"
#include "Adverb.h"
#include "Adjective.h"
#include "Preposition.h"
#include "ProperNoun.h"
#include "NounAndVerb.h"
#include "MiscWord.h"
#include "Dictionary.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include <random>
#include <time.h>
#include <map>


//utility functions, not externally accessable.

//check the passed in string contains the passed in char at some point.
bool hasChar(std::string s, char c){
	for (char ch : s){
		if (ch == c)return true;
	}
	return false;
}
//check the string contains any of the passed in characters fromthe string
bool hasAnyChar(std::string s, std::string charstring){
	for (char c : charstring){
		if (hasChar(s, c))return true;
	}
	return false;
}
//check for punctuation
bool hasPunct(std::string s){
	for (char c : s){
		if (ispunct(c)&&c!='-') return true;
	}
	return false;
}
//returns a string with the specified characters removed
std::string removeChar(std::string in, char removeChar){
	if (!hasChar(in, removeChar))return in;
	std::string ret;
	for (char c : in){
		if (c != removeChar)ret += c;
	}
	return ret;
}
//check the passed in text contains all of the passed in characters. 'abc','a' returns true, 'abc' 'ad' returns false.
bool hasAllChars(std::string text, std::string charList){
	for (char c : charList){
		if (!hasChar(text, c))return false;
	}
	return true;
}

//Count the number of times the passed in character appears in the passed in string
int countChar(std::string s, char c){

	int count = 0;
	for (char ch : s){
		if (ch == c)count++;
	}
	return count;
}

//loads the dictionary, currently a static refrence to the file, might make it more flexible if the need arises.
void Dictionary::loadDictionary(){
	std::vector<std::string>types({ "n", "v","adv","adj","prep","pn","n_and_v", "misc" });
	//file is assumed to be at data/dictionary.txt relative to the executable. 
	//This could pose a problem at some point, should check to see it's there first.
	std::ifstream inStream("data/dictionary.txt");
	std::string buffer;
	int subCount = 0;
	//use a vector to hold the string lines
	std::vector<std::string> s;
	//go through each line in the file
	while (std::getline(inStream, buffer)){
		//add the line to the temp vector
		s.push_back(buffer);
		subCount++;
		//once the count is at 4, because we know the structure of the file, we can add the correct type to the array
		if (subCount == 4){
			//basic input error checking, verbosity here is better
			try{
				//catch a zero-length word
				if (s[0].length() == 0)	throw "Zero-length word in dictionary.";
				//catch spaces in word param
				if (s[0].find(' ') != std::string::npos) throw "Found space in word parameter.";
				//catch numbers in word
				if (hasAnyChar(s[0],"0123456789")) throw "Found digit in word.";
				//catch punctuation in word
				if (hasPunct(s[0])) throw "Found punctuation in word.";
				//catch anaccounted for item in wordtype line
				if (std::find(types.begin(), types.end(), s[2]) == types.end()) throw "Bad word type.";
				//catch non empty spacer
				if (s[3].length() != 0) throw "Non zero length spacer line.";
			}
			catch (char* c){
				printf("Error section line: %d\n", (totalWords * 4)+1);
				throw c;
			}
			totalWords++;
			//here comes the mighty IF tree
			if (s.at(2).compare("n") == 0){
				wordArray.push_back(std::make_unique<Noun>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("v") == 0){
				wordArray.push_back(std::make_unique<Verb>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("adv") == 0){
				wordArray.push_back(std::make_unique<Adverb>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("adj") == 0){
				wordArray.push_back(std::make_unique<Adjective>(s.at(0),  s.at(1)));
			}
			if (s.at(2).compare("prep") == 0){
				wordArray.push_back(std::make_unique<Preposition>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("pn") == 0){
				wordArray.push_back(std::make_unique<ProperNoun>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("n_and_v") == 0){
				wordArray.push_back(std::make_unique<NounAndVerb>(s.at(0), s.at(1)));
			}
			if (s.at(2).compare("misc") == 0){
				wordArray.push_back(std::make_unique<MiscWord>(s.at(0),  s.at(1)));
			}
			s.erase(s.begin(), s.end());
			subCount = 0;
		}
	}
}

//Searches for the word supplied.
Word* Dictionary::findWord(std::string word){
	//linearsearch
	/*
	for (int i = 0; i < totalWords; i++){
		if (word.compare(wordArray[i]->getWord()) == 0)return wordArray[i].get();
	}
	return NULL;
	*/

	//Binary search
	//fix hyphens, because the hyphens used in the dict are not handled well in this search type
	word = removeChar(word, '-');
	//Set the left and right boundaries
	int l = 0;
	int r = getTotalNumberOfWords()-1;
	//while the searched word is within the boundaries
	while (l <= r){
		//find the middle point
		int m = (l + r) / 2;
		//check to see if it's what we are looking for
		std::string testWord = removeChar(wordArray[m]->getWord(),'-');
		
		int comp = word.compare((char)tolower(testWord[0])+testWord.substr(1)); //compare lower case version
		//if it is, return the pointer to it
		if (comp == 0)
			return wordArray[m].get();
		//if the word is alphabetically before the one we compared, change the upper boundary
		if (comp < 0 )
			r = m - 1;
		//likewise, if it's below, then change the lower boundary
		if (comp > 0)
			l = m + 1;
	}
	//if the boundaries don't make sense (lower bigger than higher) we didn't find it. Return Null.
	return NULL;



}
Word* Dictionary::basicTask1(std::string word){
	//simple wrapper for findWord, making the task stuff easier to write
	return findWord(word);
}
//searches for words with more than 3 'z's.
std::vector<std::string> Dictionary::basicTask2(){
	//initialise the return list
	std::vector<std::string> ret;
	//roll over each word in the dictionary
	for (auto const& w : wordArray){
		//if the counted number of z's is higher than 3, add it to the return list
		if (countChar(w->getWord(), 'z') > 3)ret.push_back(w->getWord());
	}
	//return the results
	return ret;
}
//searches for words that have the letter 'q' without a trailing 'u'.
std::vector<std::string> Dictionary::basicTask3(){
	//initialize the return vector
	std::vector<std::string> ret;
	//roll over each word in the array
	for (auto const& w : wordArray){
		//check each character of the word. 
		//todo This might be a good time to do a simple 'check if contains letter q'.
		for (int i = 0; i < w->getWord().length(); i++){
			//if the character looked at is 'q', check the next character, 
			//if it's not 'u' push it onto the return list
			if (w->getWord()[i] == 'q'&&w->getWord()[i + 1] != 'u') ret.push_back(w->getWord());
		}
	}
	//return the results
	return ret;

}
//searches for words that are both a noun and a verb.
std::vector<std::string>  Dictionary::basicTask4(){
	std::vector<std::string> ret;
	//roll over each word
	for (auto const& w : wordArray){
		//if it's both a noun and verb, push it onto the list
		if (w->isNoun() && w->isVerb())ret.push_back(w->getWord());
	}
	//return results
	return ret;
}
//searches for palindromes.
std::vector<std::string>  Dictionary::basicTask5(){
	std::vector<std::string> ret;
	//roll over each word
	for (auto const& w : wordArray){
		//check the palindrome method, if true, add to return list
		if (w->isPalindrome())ret.push_back(w->getWord());
	}
	//return the list
	return ret;
}
//finds the word(s) with the highest scrabble score.
std::vector<std::string>  Dictionary::basicTask6(){
	//since there might be more than one, initialise a return container
	std::vector<std::string> ret;

	//find the highest score
	//initialise the high score
	int highS = 0;
	//roll over each word
	for (auto const& w : wordArray){
		//check the score. If it's higher than recorded, replace with this score, add words to container
		int testScore = w->calculateScrabbleScore();
		if (testScore < highS){
			continue;
		}
		if (testScore > highS){
			highS = w->calculateScrabbleScore();
			//if it's higher, clear the container
			ret.clear();
		}
		//if it's equal, add it to the container
		if (testScore == highS){
			ret.push_back(w->getWord());
		}
	}
	//return results
	return ret;
}
//finds the word with the highest score to length ratio.
std::string Dictionary::basicTask7(){
	//initialize the highscore container
	double ratio = 0.00;
	//initialise the return value
	std::string ret;
	//roll over each word in the dictionary
	for (auto const& w : wordArray){
		//get the score/len ratio
		int score = w->calculateScrabbleScore();
		if (score == 0) continue; //MUST NOT DIVIDE BY ZERO, BAD THINGS HAPPE-
		double tempRatio = score / w->getWord().length();
		//if the ratio is higher
		if (tempRatio > ratio){
			//set the new high ratio threshold
			ratio = tempRatio;
			//set the return word to the word
			ret = w.get()->getWord();
		}
	}
	return ret;

}

//finds the word that could be used in scrabble with the highest score, 
//based on the letters provided.
std::string Dictionary::intermediateTask1(std::string letters){
	//initialise containers
	std::string ret;
	std::vector<Word*> list;
	bool flag;
	//for clarity, this is a two part process. Get the words that fit, then pick the top score.

	//Get the words that fit:
	//roll over each word in the dictionary
	for (auto const& w: wordArray){
		//roll over each character
		for (char c : w->getWord()){
			flag = false;
			//conditionals for failure - if any of these are not true, stop looking at this word.
			if (
				//first, check to see if the passed in letters contain the character of the word OR
				countChar(letters, c) == 0 ||
				//check to see if the number of characters in the passed in string that match are
				//  lower than the number of times the character appears in the word.
				countChar(letters,c)< countChar(w->getWord(), c)) break;
			//assuming it makes it this far, the flag is set to true
			flag = true;
		}
		//push the word onto the potential pool - this should only happen when the failure conditions are not met
		if(flag)list.push_back(w.get());
	}

	//Pick the top score
	//Initialise the score comparison
	int maxScore = 0;
	//roll over each word in the potential pool
	for (auto& w : list){
		//check to see if the top found score is beaten
		if (w->calculateScrabbleScore() > maxScore){
			//if it's beaten, replace with better score
			maxScore = w->calculateScrabbleScore();
			//assign winning word to return value
			ret = w->getWord();
		}
	}
	//return result
	return ret;
}
//Finds all words that have the same last 3 letters as the provided word
std::vector<std::string> Dictionary::intermediateTask2(std::string letters){
	//only interested in the last 3 letters
	std::string last3 = letters.substr(letters.length() - 3, 3);
	//create return list
	std::vector<std::string> ret;
	//roll over each word
	for (auto& w : wordArray){
		//check the word is of sensible length ( 'g' can't rhyme with 'fishing')
		if (w->getWord().length() > 2 &&
			//compare the last 3 letters of the passed in value with the last 3 of the tested word
			last3.compare(w->getWord().substr(w->getWord().length() - 3, 3)) == 0)
				//if they match, add the word to the results
				ret.push_back(w->getWord());
	}
	//return the results
	return ret;
}
//Finds all anagrams of the provided letters.
std::vector<std::string> Dictionary::intermediateTask3(std::string word){
	//initialise the return container.
	std::vector<std::string> list;
	//initialise the flag
	bool flag;
	//roll over each word
	for (auto const& w : wordArray){
		//if the word isn't the same length, don't bother looking
		if (w->getWord().length() != word.length()) continue;
		//check each letter in the test word
		for (char c : w->getWord()){
			//set our flag to false
			flag = false;
			//first check to make sure the character count in the test word from the word supplied isn't 0
			if (countChar(word, c) == 0 ||
				//check to make sure that the count of each character is exactly the same
				countChar(word, c) != countChar(w->getWord(), c)) break;
			//if the tests are passed the flag is set to true
			flag = true;
		}
		//put the word on the return list if it passes the tests
		if(flag)list.push_back(w->getWord());
	}
	//return the results
	return list;
}

//simply selects a word from the dictionary at random, and provides it to the hangman game
std::string Dictionary::advancedTask1(){
	//seed random with system time (random -enough-)
	srand(time(NULL));
	//select a random word
	return wordArray[rand() % wordArray.size()]->getWord();

}
//Creates a list of 10 new english words based on relationships in the dictionary
 std::vector<std::string> Dictionary::advancedTask2(){
	 /* 
		this is a VERY resource intensive implementation, though it shows the concept quite easily.
		It could be done using counts, instead of the actual digraph object, to use significantly less memory
	 */
	 //sets the seed for rand to system time, random -enough-
	 srand(time(NULL));
	 //assuming that proper nouns are to be handled as lower case
	std::vector<std::string> ret;
	//alphabet vector to create the digraphs, includes hyphen
	char alpha[] = "abcdefghijklmnopqrstuvwxyz-";
	std::vector<std::string> digraphList;
	//digraph map starter
	std::map< std::string, std::vector<std::string>> digraphMap;

	//create map of digraphs & vectors

	// for each letter in the alphabet container
	for (int i = 0; i < sizeof(alpha); i++){
		//get a pair of letters
		for (int j = 0; j < sizeof(alpha); j++){
			//create a map of digraph ('aa','ab' etc) and vector
			digraphMap[std::string( {alpha[i],alpha[j]})] = std::vector<std::string>();
		}
	}

	//fill the digraph map with data from the dictionary
	//at the same time, collect the start of each word and put it into a vector (so we can create a statistically likely start-of-word)
	std::vector<std::string> startVector;
	//counter, for telling the user how -done- we are
	int ctr = 0;
	printf("[*] Gathering statistics\n");
	//for each word in dict
	for (auto const& w : wordArray){
		//put the word into a string so it's easier to work with
		std::string word = w->getWord();
		//handle the proper nouns, because capitalisation is hard
		word = (char)tolower(word[0]) + word.substr(1);
		//make sure it's a valid word to assess (if the size is 1, it would skew the data and break the program)
		if (word.length() < 2)continue;
		//add first two letters to start vector
		startVector.push_back(word.substr(0, 2));
		//for letter in word, until last sensible letter (length -2)
		for (int i = 0; i < word.length() - 2; i++){
			std::string key = { word[i], word[i + 1] };
			std::string value = { word[i + 1], word[i + 2] };
			//add to digraph map
			digraphMap.at(key).push_back(value);
		}
		//it will actually be quicker with no output here, but it feels quicker because you can see something happening
		int done = (ctr / (double) totalWords)*100;
		std::cout <<"[*] " << done << "% complete\r";
		ctr++;
	}
	printf("[*] Statistics done, creating words!\n");
	//create the words based on the mapping

	//loop 10 times for 10 words
	for (int i = 0; i < 10; i++){
		//pick random first two letters from start vector
		std::string newWord = startVector[rand() % startVector.size() - 1];
		//construct a new word of length between 4 and 12 letters
		while (newWord.length() < rand() % 8 + 4){
			int length = newWord.length();
			//get the relevant pool of choices from the last two letters of current word
			std::vector<std::string> digraphPool = digraphMap.at({ newWord[length - 2], newWord[length - 1] });
			//get random digraph from last two letters of current word
			std::string randomDigraph = digraphPool[rand() % digraphPool.size() - 1];
			//append last character of digraph
			newWord.push_back(randomDigraph[1]);
		}
		//append 'word' to return list, only if word doesn't appear in dictionary
		if (findWord(newWord) != NULL){
			//if it's found, pretend we never went through this loop (although it is a sign that the word is englishy, so put a breakpoint here if interested)
			i--;
			continue;
		}
		//we made a new word! \o/
		std::cout <<"[*] "<< ret.size() << "/" << 10 << '\r';
		ret.push_back(newWord);
	}

	//return machine-made word list
	return ret; //there is massive overhead here, and for some reason de-allocation of memory here causes a long wait.



}