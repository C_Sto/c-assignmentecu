#ifndef WORD_H
#define WORD_H

#include <string>



class Word{

protected:
	//the word in the dictionary
	std::string word;
	//definition found in dictionary
	std::string definition;

public:
	Word(){}
	virtual ~Word(){}
 //public default constructor
	//full featured constructor is best constructor for prosperity of greatest nation of kazachstan
	Word(std::string sWord, std::string sDefinition){
		setWord(sWord);
		setDefinition(sDefinition);
	}
	//setters
	void setWord(std::string w){ word = w; }
	void setDefinition(std::string d){ definition = d; }

	//getters
	virtual std::string getWord();
	virtual std::string getDefinition(){ return definition; }

	//methods
	virtual int calculateScrabbleScore();
	bool isHyphenated();
	bool isPalindrome();
	virtual bool isVerb();
	virtual bool isNoun();
};

#endif