#ifndef ADVERB_H
#define ADVERB_H

#include <string>
#include "word.h"

class Adverb : public Word{


public:
	Adverb(){};

	//Standard constructor that initializes all fields.
	Adverb(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	std::string getDefinition(){ return "(adv.) " + definition; }
};




#endif