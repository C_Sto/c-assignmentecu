#ifndef NOUN_H
#define NOUN_H

#include <string>
#include "word.h"

class Noun :virtual public Word{

public:
	Noun(){};
	//Standard constructor that initializes all fields.
	Noun(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	//presumably this should return true, one would think.
	bool isNoun();

	std::string getDefinition(){ return "(n.) " + definition; }
};



#endif
