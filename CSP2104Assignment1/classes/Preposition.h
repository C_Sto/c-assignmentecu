#ifndef PREPOSITION_H
#define PREPOSITION_H

#include <string>
#include "MiscWord.h"


class Preposition :public virtual MiscWord{

public:
	Preposition(){};
	//Standard constructor that initializes all fields.
	Preposition(std::string w, std::string d){
		MiscWord::setWord(w);
		MiscWord::setDefinition(d);
	}

	//bypass the Miscword not-scrabble relevant score
	int calculateScrabbleScore(){
		return Word::calculateScrabbleScore();
	}

	std::string getDefinition(){ return "(prep) " + definition; }
};
#endif