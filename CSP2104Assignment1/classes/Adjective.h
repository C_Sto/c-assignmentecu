#ifndef ADJECTIVE_H
#define ADJECTIVE_H

#include <string>
#include "word.h"

class Adjective : public Word{

public:

	Adjective(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	std::string getDefinition(){ return "(adj.) " + definition; }

};



#endif