#include <string>
#include <map>
#include <algorithm>
#include "word.h"

//just like it says on the tin
int Word::calculateScrabbleScore(){
	//short-circuits go first, in theory this should (sometimes) save a little bit of time
	if (isHyphenated())return 0; //Any word with a hyphen returns a zero score, not allowed

	//initialize the score map
	std::map<char, int> scoreMap = { { 'A', 1 }, { 'B', 3 }, { 'C', 3 }, { 'D', 2 }, { 'E', 1 },
									 { 'F', 4 }, { 'G', 2 }, { 'H', 4 }, { 'I', 1 }, { 'J', 8 },
									 { 'K', 5 }, { 'L', 1 }, { 'M', 3 }, { 'N', 1 }, { 'O', 1 },
									 { 'P', 3 }, { 'Q', 10 }, { 'R', 1 }, { 'S', 1 }, { 'T', 1 },
									 { 'U', 1 }, { 'V', 4 }, { 'W', 4 }, { 'X', 8 }, { 'Y', 4 }, 
									 { 'Z', 10 } };
	//initialize the return value
	int ret = 0;
	//cycle through each letter in the word
	for (char c : getWord()){
		//get the letter score from the map, and add to the return value
		ret += scoreMap.at(toupper(c));
	}
	//return with the total score
	return ret;
}

bool Word::isHyphenated(){
	//check for any occurences of '-' if found, return true
	if (std::string::npos != word.find('-', 0)) return true;
	//ortherwise return false
	return false;
}

bool Word::isPalindrome(){

	//initialize the comparison objects
	std::string rightWay = getWord();
	std::string wrongWay = getWord();
	//flip the wrongWay object
	std::reverse(wrongWay.begin(), wrongWay.end());
	//if they match, it is a palindrome
	if (rightWay.compare(wrongWay)==0)return true;
	//otherwise false
	return false;
}

bool Word::isVerb(){
	// this method is overridden in the 'Verb' class
	return false;
}

bool Word::isNoun(){
	// this method is overridden in the 'Noun' class
	return false;
}

std::string Word::getWord(){
	return word;
}