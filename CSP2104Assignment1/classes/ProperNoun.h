#ifndef PROPERNOUN_H
#define PROPERNOUN_H

#include <string>
#include "word.h"


class ProperNoun : virtual public Word{

public:
	ProperNoun(){};
	//Standard constructor that initializes all fields.
	ProperNoun(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	std::string getWord(){
		return (char)toupper(word[0]) + word.substr(1);
	}

	//as per assignment instructions, Proper noun is not valid scrabble
	int ProperNoun::calculateScrabbleScore(){ return 0; }

	std::string getDefinition(){ return "(pn.) " + definition; }
};
#endif