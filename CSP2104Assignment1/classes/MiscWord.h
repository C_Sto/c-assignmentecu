#ifndef MISCWORD_H
#define MISCWORD_H

#include <string>
#include "word.h"


class MiscWord : public Word{

public:
	MiscWord(){};
	//Standard constructor that initializes all fields.
	MiscWord(std::string w, std::string d){
		setWord(w);
		setDefinition(d);
	}

	//as per assignment instructions, miscWords don't work in scrabble
	int calculateScrabbleScore(){ return 0; }
	std::string getDefinition(){ return "(misc.) " + definition; }
};
#endif