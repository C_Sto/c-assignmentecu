

#include <iostream>
#include <string>
#include <sstream>

#include "main.h"
#include "tasks\Tasks.h"


//This is the entry point to the program, and where the control logic lies



int main(){
	//create the dictionary object locally, it's quicker and less likely to leak memory
	Dictionary diction;
	
	//pointer in the same scope, to avoid memory leaks & enable passing refrences to functions
	Dictionary* dict = &diction;
	
	//initialize interaction variables & logic flags & stuff
	bool mainFlag = true;
	std::string mainCont;
	std::string task;
	std::string cat;
	//let the user know that something is happening, otherwise they might think the program is broken.
	printf("[*] Loading dictionary, please wait..\n");
	try{
		dict->loadDictionary();
	}
	catch (char* c){
		printf("%s\n",c);
		printf("Aborting.");
		system("pause");
		return 0;
	}
	//let the user know that the program (supposedly) did something
	printf("[*] Load complete!\n");
	//check the programs loop flag for an exit condition
	while (mainFlag){
		//(re) init the choice & invalid flags
		int choice = NULL;
		bool invalid = false;
		printf(" - Main Menu -\n");
		//included category hint in this message, makes life easier for the user
		printf("[>] Select a category (b)asic/(i)ntermediate/(a)dvanced: ");
		//getline works better than cin, less strange behaviour. Assigning the category variable
		std::getline(std::cin, cat);
		//force the variable to lowercase, also select only first character, incase of sneaky people are trying to break it
		cat = tolower(cat[0]);
		//depending on the category, show different task options
		if (cat.compare("b") == 0){
			printf("[>] select a task [1-7]: ");
		}
		else if (cat.compare("i") == 0 || cat.compare("a") == 0){
			printf("[>] select a task [1-3]: ");
		}
		else invalid = true;
		//assigning the task value if valid category input is found
		if (!invalid){
			std::getline(std::cin, task);
			std::stringstream sstream(task);
			//turn it into an int for the switch. Intentionally not checking for errors here, they will be caught by the default switch.. I think
			sstream >> choice;
		}
		//in the case of invalid cateogry input (naughty naughty) send a non category to the switch
		else{
			choice = -1;
		}
		/*switch to get the desired task behaviour
		  Switch is used on the task number mostly because it doesn't work on strings,
		  but also because it's tidier to have 1 switch statement with tiny if's,
		  rather than 3 if's and a nested switch for each category
		*/
		switch (choice){
		case 1:
			//since only the first few cases take categories into consideration, this is the tidiest way of representing them
			if (cat.compare("b") == 0) basicTask1(dict);
			else if (cat.compare("i") == 0) intermediateTask1(dict);
			else if (cat.compare("a") == 0) advancedTask1(dict);
			else choice = -1;
			break;
		case 2:
			if (cat.compare("b") == 0)basicTask2(dict);
			else if (cat.compare("i") == 0)intermediateTask2(dict);
			else if (cat.compare("a") == 0)advancedTask2(dict);
			else choice = -1;
			break;
		case 3:
			if (cat.compare("b") == 0)basicTask3(dict);
			else if (cat.compare("i") == 0)intermediateTask3(dict);
			else if (cat.compare("a") == 0)advancedTask3(dict);
			else choice = -1;
			break;
		case 4:
			if (cat.compare("b") == 0)basicTask4(dict);
			else choice = -1;
			break;
		case 5:
			if (cat.compare("b") == 0)basicTask5(dict);
			else choice = -1;
			break;
		case 6:
			if (cat.compare("b") == 0)basicTask6(dict);
			else choice = -1;
			break;
		case 7:
			if (cat.compare("b") == 0)basicTask7(dict);
			else choice = -1;
			break;
		//set the default behaviour, incase someone were to try and break it, or didn't enter a number into the task choice
		default: printf("[-] Invalid choice. Please try again.\n");
			break;
		}
		//after the desired task has finished completion, offer the chance to do another one, so that the user doesn't have to re-load the dictionary for each task. How nice.
		printf("[>] Pick another task? (y/n): ");
		std::getline(std::cin, mainCont);
		//check for an exit condition.
		if (mainCont.compare("n") == 0 || mainCont.compare("N") == 0){
			mainFlag = false;
		}
		//clear the console, it looks tidier.
		system("cls");
	}
	//exit condition found, time to exit
	printf("[*] Program Exit..\n");
	system("pause");
	return 0;
}