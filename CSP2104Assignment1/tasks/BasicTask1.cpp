#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>
#include <sstream>
//Basic tast 1: scrabble word search
// prints out the word (if found), definition, and scrabble score.
void basicTask1(Dictionary* dict){
	//flush the screen, for tidy
	system("cls");
	//let the user know what's going on, and what to do
	printf("- Basic Task 1 -\n-  -Scrabble Word Lookup -\n");
	printf("[>] Enter a word to search: ");

	std::string in;
	std::getline(std::cin, in);
	//make sure input 'works'
	if (in.length() > 0){
		//see if we can find the word
		Word *displ = dict->basicTask1(in);
		//if we can't, or the score is 0, let the user know the bad news
		if (displ == nullptr) printf("[-] Word not found.\n");
		//otherwise, let them know the good news
		else{
			std::cout << "[+] Definition: " << displ->getDefinition() << '\n';
			if (displ->calculateScrabbleScore() > 0) std::cout << "[+] Scrabble score: " << displ->calculateScrabbleScore() << '\n';
			else printf("[+] Not valid in scrabble.\n");
		}
	}
	else printf("[-] You have to enter at least one character.\n");

	printf("[*] Done.\n");
}