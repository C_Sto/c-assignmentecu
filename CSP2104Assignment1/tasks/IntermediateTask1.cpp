#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void intermediateTask1(Dictionary* dict){
	system("cls");
	printf("- Intermediate Task 1 -\n-  -Scrabble helper -\n");
	std::string buffer;
	printf("[>] Enter letters: ");
	std::getline(std::cin, buffer);
	std::string outp = dict->intermediateTask1(buffer);
	if (outp.length() > 0){
		printf("[*] Word containing only letters provided, with highest score: \n");
		std::cout << "  [+] "<< outp << '\n';
		std::cout << "  [+] With score: " << dict->basicTask1(outp)->calculateScrabbleScore() << '\n';
	}
	else{
		printf("[-] No word found :(\n");
	}
	printf("[*] Finished.\n");

}