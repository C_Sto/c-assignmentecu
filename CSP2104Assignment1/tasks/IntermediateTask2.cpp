#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void intermediateTask2(Dictionary* dict){
	system("cls");
	std::string buffer;
	printf("- Intermediate Task 2 -\n-  -'Rhyming' words -\n");
	printf("[>] Enter a string of letters: ");
	std::getline(std::cin, buffer);
	std::vector<std::string> outp = dict->intermediateTask2(buffer);
	if (outp.size() > 0){
		printf("[*] List of words with same 3 letters at the end: \n");
		for (std::string s : outp){
			std::cout << "  [+] " << s << '\n';
		}
	}
	else{
		printf("[-] None found :(\n");
	}
	printf("[*] Finished.\n");

}