#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>
#include <fstream>


//Makes the string to display to the player in place of the word
std::string makeDisplayWord(std::string word, std::string guesses){
	std::string ret;
	for (char c : word){
		//reaplace each letter with a '.' unless it's a '-', and unless it's been found.
		if (guesses.find(c) == std::string::npos && c != '-'){
			ret += '.';
		}
		else if(c == '-'){
			ret += '-';
		}
		else{
			ret += c;
		}
	}
	return ret;
}

//Game of hangman
void advancedTask1(Dictionary* dict){
	system("cls");
	printf("- Advanced Task 1 -\n-  -Hangman -\n");
	std::string guessedLetters;
	std::string buffer;
	printf("[*] Selecting random word..\n");
	//pick random word
	std::string hangmanWord = dict->advancedTask1();
	
	int lettersRemaining = hangmanWord.length();
	//handle the case of hyphens. Messy, and pretty inefficient, but does the job
	if (hangmanWord.find('-')){
		for (char c : hangmanWord){
			if (c == '-') lettersRemaining--;
		}
	}
	printf("[*] Selected!\n");

	int guesses = 6;
	//start game
	while (guesses > 0){
		printf("[*] Word: ");
		//show word as .'s, and number of guesses
		std::cout<<makeDisplayWord(hangmanWord, guessedLetters)<<'\n';
		printf("[*] Guesses remaining:");
		//number of guesses remaining
		std::cout << guesses << '\n';
		printf("[>] Enter Letter to guess");
		std::cout << "(" << guessedLetters << "): ";
		//prompt for letter
		std::getline(std::cin, buffer);
		//error check the letter
		if (buffer.length() > 1 || !isalpha(buffer[0])){
			//clear the screen, or it gets messy 
			system("cls");
			std::cout << "[-] Entry error, try again.\n";
			continue;
		}
		//error check letter isn't already guessed
		if (guessedLetters.find(buffer) != std::string::npos){
			//clear the screen, or it gets messy 
			system("cls");
			printf("[-] Letter already guessed, pick another.\n");
			continue;
		}
		//append guess to guess list
		guessedLetters.append(buffer);
		//if letter in word, 
		if (hangmanWord.find(buffer) != std::string::npos){
			//decrement letters remaining by number of times letter appears in hangmanword
			for (char c : hangmanWord){
				if (buffer[0] == c) lettersRemaining--;
				system("cls");
				printf("[+] Letter found in word\n");
			}

			//if letters remaining = 0
			if (lettersRemaining == 0){
				//win
				//clear the screen, or it gets messy 
				system("cls");
				printf("[+] Winner! Word was: ");
				std::cout << hangmanWord<<'\n';
				break;
			}
		}
		else{
			system("cls");
			printf("[-] Letter not found in word\n");
			//if not, decrement guesses by 1
			guesses--;
			if (guesses == 0){
				//clear the screen, or it gets messy 
				system("cls");
				printf("[-] You lost! The word was: ");
				std::cout << hangmanWord << '\n';
			}
		}
	}
	std::cout << "[*]Definition: " << dict->findWord(hangmanWord)->getDefinition() << '\n';
	printf("[*] Finished.\n");

}