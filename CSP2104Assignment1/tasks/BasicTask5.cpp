#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void basicTask5(Dictionary* dict){
	system("cls");
	printf("- Basic Task 5 -\n-  -Palendrome lookup -\n");
	printf("[*] Listing all words that are palindromes..\n");
	for (std::string s : dict->basicTask5()){
		std::cout <<"  [+] "<< s << '\n';
	}
	printf("[*] Finished.\n");

}