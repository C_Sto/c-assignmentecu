#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void basicTask7(Dictionary* dict){
	system("cls");
	printf("- Basic Task 7 -\n-  -Highest Scrabble score to length lookup -\n");
	printf("[*] The word with the highest score to length ratio is:\n");
	std::string word = dict->basicTask7();
	std::cout << "  [+] Word:"<< word<<'\n';
	std::cout << "  [+] Score:" << dict->findWord(word)->calculateScrabbleScore() << '\n';
	std::cout << "  [+] Length:" << word.length() << '\n';
	printf("[*] Finished.\n");

}