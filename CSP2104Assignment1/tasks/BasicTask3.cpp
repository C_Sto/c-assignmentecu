#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>

//find words with q that don't have letter u following it
void basicTask3(Dictionary* dict){
	//let the user know what's going on
	system("cls");
	printf("- Basic Task 3 -\n-  -Find words with 'q' but no 'u' -\n");
	printf("[*] Finding all words that contain the letter 'q',\n[*] without the letter 'u' following..\n");
	//print the result
	for (std::string s : dict->basicTask3()){
		std::cout <<"  [+] "<< s<<'\n';
	}
	printf("[*] Finished.\n");


}