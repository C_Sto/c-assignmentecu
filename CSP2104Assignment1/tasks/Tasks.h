#ifndef TASKS_H
#define TASKS_H

#include "..\main.h"


void basicTask1(Dictionary* dict);
void basicTask2(Dictionary* dict);
void basicTask3(Dictionary* dict);
void basicTask4(Dictionary* dict);
void basicTask5(Dictionary* dict);
void basicTask6(Dictionary* dict);
void basicTask7(Dictionary* dict);


void intermediateTask1(Dictionary* dict);
void intermediateTask2(Dictionary* dict);
void intermediateTask3(Dictionary* dict);

void advancedTask1(Dictionary* dict);
void advancedTask2(Dictionary* dict);
void advancedTask3(Dictionary* dict);

#endif