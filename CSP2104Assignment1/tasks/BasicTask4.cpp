#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void basicTask4(Dictionary* dict){
	system("cls");
	printf("- Basic Task 4 -\n-  -Words that are both nouns and verbs -\n");
	printf("[*] Listing all words that are both an noun, and a verb..\n");
	for (std::string s : dict->basicTask4()){
		std::cout <<"  [+] "<< s << '\n';
	}
	printf("[*] Finished.\n");

}