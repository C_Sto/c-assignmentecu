#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void basicTask6(Dictionary* dict){
	system("cls");
	printf("- Basic Task 6 -\n-  -Highest Scrabble score lookup -\n");
	printf("Listing all words with the highest scrabble score:\n");
	for (std::string s : dict->basicTask6()){
		std::cout <<"  [+] "+ s << '\n';
	}
	printf("[*] Finished.\n");

}