#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <algorithm>

bool score_test(Word* a, Word* b){ return a->calculateScrabbleScore() > b->calculateScrabbleScore(); }

void advancedTask3(Dictionary* dict){
	std::map<char, std::vector<Word*>> characterWordMap;
	std::vector<Word*> wordVec;
	
	system("cls");
	printf("- Advanced Task 3 -\n-  -Random letter Wordsearch -\n");
	std::string buffer;
	printf("[*] Reading in random_letters.txt..\n");
	std::ifstream inStream("data/random_letters.txt");
	std::getline(inStream, buffer);
	int index = 0;
	printf("[*] Searching for words..\n");
	while (index <= buffer.length()-3){
		std::cout << "                             \r";
		std::cout << (int)(index / (double)buffer.length() * 100) << "% complete\r";
		std::string window = buffer.substr(index, 3);
		if (Word* w = dict->findWord(window)){

			/* DELETE DUE TO TASK RE-STRUCTURE
			std::cout << "                             \r";
			printf("  [+] Found word: ");
			std::cout << w->getWord() << '\n';
			std::cout << "  [+] " << w->getDefinition() << '\n';
			printf("  [+] Scrabble score: ");
			std::cout << w->calculateScrabbleScore() << '\n';
			*/
			//add the word to the map based on the first character
			characterWordMap[tolower(w->getWord()[0])].push_back(w);
		}
		index++;

	}
	//ask user for desired character
	bool flag = true;
	char choice;
	while (flag){
		std::cout << "                             \r";
		printf("[>] Enter character:");
		std::getline(std::cin, buffer);
		//similar to picking category in main, validate input to a single alphabetic character
		choice = tolower(buffer[0]);
		if (!isalpha(choice) || buffer.size() > 1){ printf("[-] Entry error, please try again.\n"); continue; }
		flag = false;
	}
	//sort for desired output
	std::sort(characterWordMap[choice].begin(), characterWordMap[choice].end(), score_test);
	//display the output, for prosperity
	for (Word* w : characterWordMap[choice]){
		printf("\n");
		printf("  [+] Found word: ");
		std::cout << w->getWord() << '\n';
		std::cout << "  [+] " << w->getDefinition() << '\n';
		printf("  [+] Scrabble score: ");
		std::cout << w->calculateScrabbleScore() << '\n';
	}
	printf("Finished.\n");

}