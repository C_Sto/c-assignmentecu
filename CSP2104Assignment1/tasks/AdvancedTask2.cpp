#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>
#include <vector>

void advancedTask2(Dictionary* dict){
	system("cls");
	printf("- Advanced Task 2 -\n-  -New words -\n");
	printf("[*] Generating 10 new words from dictionary, please wait..\n[*] (this may take a while)\n");
	std::vector<std::string> wordList = dict->advancedTask2();
	printf("[*] Done! here they are:\n");
	for (std::string s : wordList){
		std::cout <<"  [+] " << s << '\n';
	}
	printf("[*] Done.\n");



}



