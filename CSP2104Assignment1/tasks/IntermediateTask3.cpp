#include "Tasks.h"

#include "..\main.h"

#include <iostream>
#include <string>


void intermediateTask3(Dictionary* dict){
	system("cls");
	std::string buffer;
	printf("- Intermediate Task 3 -\n-  -Anagram finder -\n");
	printf("[>] Enter a word: ");
	std::getline(std::cin, buffer);
	std::vector<std::string> outp = dict->intermediateTask3(buffer);
	if (outp.size() > 0){
		printf("[*] List of anagrams: \n");
		for (std::string s : outp){
			std::cout << "  [+] " << s << '\n';
		}
	}
	else{
		printf("[-] None found :(\n");
	}
	printf("[*] Finished.\n");

}