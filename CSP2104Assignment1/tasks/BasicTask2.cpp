#include "Tasks.h"

#include "..\main.h"

#include <iostream>


void basicTask2(Dictionary* dict){
	system("cls");
	//let the user know what is going on	
	printf("- Basic Task 2 -\n-  -Find all words with 4 or more 'z's -\n");
	printf("[*] Finding all words with more than 3 z's (4 or more)..\n");
	//create the container to collect all of the found words
	std::vector<std::string> vec= dict->basicTask2();
	//print each word out one by one to the user
	for (std::string s : vec){
		std::cout <<"  [+] "<< s << '\n';
	}
	//let the user know that it's the end of the list
	std::cout << "[*] Finished. \n";

}